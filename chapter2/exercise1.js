// http://eloquentjavascript.net/02_program_structure.html#p_pP646YLlGy

var line    = "",
    suffix  = "#",
    stop_at = "#######";

while (line < stop_at)
  console.log(line += suffix);

