// http://eloquentjavascript.net/02_program_structure.html#p_uH3DV6RVnV

var size       = 5,
    chessBoard = "",
    blackTile  = "::",
    whiteTile  = "  ";

for (var row = 0; row < size; row++) {
  for (var col = 0; col < size; col++) {

    chessBoard += (row + col) % 2 ? blackTile : whiteTile;
    if (col == size - 1) chessBoard += "\n";

  }
}

console.log( "Behold, my", size, "by", size, "chess board:" );
console.log( chessBoard );
