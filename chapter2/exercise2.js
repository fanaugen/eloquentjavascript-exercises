// http://eloquentjavascript.net/02_program_structure.html#p_i0Cvwf75cQ 

for (var i = 1; i <= 100; i++) {
  console.log(
    "" + (i%3 ? "" : "Fizz")
       + (i%5 ? "" : "Buzz")
    || i
  )
}
