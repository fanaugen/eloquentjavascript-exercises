// http://eloquentjavascript.net/03_functions.html#p_8y74cOkS91

// count capital "B"s in a string
function countBs(str) {
  return countChar(str, "B");
}

// count the number of occurrences of character c in string str
function countChar(str, c) {
  for (var i = 0, count = 0, len = str.length; i < len; i++) {
    if (str.charAt(i) == c) count++
  }

  return count;
}

// tests
console.log(countBs("BBC"), "should be", 2);
console.log(countChar("kakkerlak", "k"), "should be", 4);
