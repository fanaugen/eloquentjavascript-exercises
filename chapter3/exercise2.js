// http://eloquentjavascript.net/03_functions.html#p_iDq2OgBOGw

function isEven(number) {
  // edge cases
  if (isNaN(number)) return undefined;
  if (number < 0) return isEven(-1 * number);

  // base case: 0 is even, 1 isn’t
  if (number < 2) return number == 0;

  // recursive case: isEven(n) == isEven(n-2)
  return isEven(number - 2);
}

// tests
console.log( isEven(50), "should be true");
console.log( isEven(75), "should be false");
console.log( isEven(-1), "should be false");
