// http://eloquentjavascript.net/03_functions.html#p_aW/Uoj4mDd

function min(a, b) {
  return a < b ? a : b;
}

// tests
console.log(min(0,  10), "should be",   0);
console.log(min(0, -10), "should be", -10);
