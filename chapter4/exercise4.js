// http://eloquentjavascript.net/04_data.html#p_xTwbRlqHNJ

function deepEqual(a, b) {
  if (!(typeof a == "object") ||
      !(typeof b == "object") ||
      a == null || b == null) return a === b;

  // deep-compare every property of obj a to obj b
  for (var prop in a) {
    if (!deepEqual(a[prop], b[prop])) return false;
  }
  return true;
}

// tests
var obj = {here: {is: "an"}, object: 2};

console.log(deepEqual(obj, obj), "should be", true);

console.log(deepEqual(obj, {here: 1, object: 2}), "should be", false);

console.log(deepEqual(obj, {here: {is: "an"}, object: 2}), "should be", true);
