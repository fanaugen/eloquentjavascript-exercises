// http://eloquentjavascript.net/04_data.html#p_7AnSuS26HF

function arrayToList(arr) {
  for (var list, rest = null, i = arr.length - 1;
       i >= 0;
       i--, rest = list)
  { list = prepend(arr[i], rest) }

  return list;
}

function prepend(elem, list) {
  return {value: elem, rest: list}
}


function listToArray(list) {
  if (!("value" in list)) return [];
  var arr = [];
  do {
    arr.push(list.value)
    list = list.rest
  } while (list);

  return arr;
}

function nth(list, n) {
  // iterative:
  // return listToArray(list)[n];
  
  // recursive:
  if (list == null) return undefined;
  if (n == 0) return list.value || undefined;
  return nth(list.rest, n-1)
}


// tests
console.log(arrayToList([10, 20]), "should be", {value: 10, rest: {value: 20, rest: null}});

var arr = [10, 20, 30];
console.log(listToArray(arrayToList(arr)), "should be", arr);

console.log(prepend(11, prepend(22, null)), "should be", {value: 11, rest: {value: 22, rest: null}});

console.log(nth(arrayToList([10, 20, 30]), 1), "should be", 20);
console.log(nth(arrayToList([1, 2, 3]), 5), "should be", undefined);
