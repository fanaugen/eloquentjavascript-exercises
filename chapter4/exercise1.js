// http://eloquentjavascript.net/04_data.html#p_fpyyiv/hm1

/* returns an Array of numbers
 * from start to end (ascending, unless step is negative)
 * step defaults to 1, so range(3,2) is empty
 */
function range(start, end, step) {
  // edge cases
  if (typeof step == "undefined") step = 1;
  if (typeof end  == "undefined") return [start];
  if (step == 0) return undefined;

  var withinRange;
  if (step > 0) {
    withinRange = function(next) {
      return next >= start && next <= end
    };
  } else { // negative step
    withinRange = function(next) {
      return next <= start && next >= end
    };
  }

  for (var result = [], next = start;
       withinRange(next);
       next += step
      ) { result.push(next) }

  return result
}

function sum(numbers) {
  for (var s = 0, i = 0, len = numbers.length;
       i < len;
       i++) s += numbers[i]
  return s
}

// tests for range
console.log(range(1,4), "should be", [1,2,3,4])
console.log(range(5,3), "should be", [])
console.log(range(9), "should be", [9])
console.log(range(9,12,8), "should be", [9])
console.log(range(0,9,0), "should be", undefined)
console.log(range(1,10,4), "should be", [1,5,9])
console.log(range(10,1,-3), "should be", [10,7,4,1])
console.log(range(3,8,-1), "should be", [])
console.log(range(5,2,-1), "should be", [5,4,3,2])

// tests for sum
console.log(sum([2]), "should be", 2)
console.log(sum([2,4,6]), "should be", 12)
console.log(sum([5,6,-1]), "should be", 10)
console.log(sum([1,2,3,4]), "should be", 10)
console.log(sum([]), "should be", 0)

// tests for both
console.log(sum(range(1,10)), "should be", 55)
console.log(sum(range(10,-2,-2)), "should be", 28)

