// http://eloquentjavascript.net/04_data.html#p_0ysB6LgssH

function reverseArray(arr) {
  for (var rev = [], i = arr.length - 1;
       i >= 0;
       i--) rev.push(arr[i]);
  return rev;
}

function reverseArrayInPlace(arr) {
  var len = arr.length;

  // push every item but the last one in reverse order
  for (var i = len - 2; i >= 0; i--) {
    arr.push(arr[i]);
  }

  // truncate the left part
  for (var i = 0; i < len - 1; i++) arr.shift();
}

// tests
console.log(reverseArray(["A", "B", "C"]), "should be",["C", "B", "A"]);
console.log(reverseArray(["X", "Y"]), "should be",["Y", "X"]);
console.log(reverseArray(["N"]), "should be",["N"]);
var arrayValue = [1, 2, 3, 4, 5];
reverseArrayInPlace(arrayValue);
console.log(arrayValue, "should be", [5, 4, 3, 2, 1]);
